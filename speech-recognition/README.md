# Spoken Digit Speech Recognition

This is a complete example of training an spoken digit speech recognition model on the "MNIST dataset of speech recognition".


## Prepare
git clone https://github.com/Jakobovski/free-spoken-digit-dataset.git
mkdir speech_recog_digit_data
cp -r free-spoken-digit-dataset/recordings speech_recog_digit_data
cd speech_recog_digit_data

Create an experiment CSV.

echo "audio_path","label" >> "spoken_digit.csv"
cd "recordings"
ls | while read -r file_name; do
   audio_path=$(readlink -m "${file_name}")
   label=$(echo ${file_name} | cut -c1)
   echo "${audio_path},${label}" >> "../spoken_digit.csv"
done
cd "../"

## train
ludwig experiment \
  --data_csv <PATH_TO_SPOKEN_DIGIT_CSV> \
  --model_definition_file model_definition_file.yaml