# Text classification

This example shows how to build a text classifier with Ludwig. It can be performed using the Reuters-21578 dataset, in particular the version available on CMU's Text Analytics course website. Other datasets available on the same webpage, like OHSUMED, is a well-known medical abstracts dataset, and Epinions.com, a dataset of product reviews, can be used too as the name of the columns is the same.

## Run
ludwig experiment \
  --data_csv text_classification.csv \
  --model_definition_file model_definition.yaml