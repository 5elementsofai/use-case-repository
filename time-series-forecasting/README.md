# Time series forecasting

While direct timeseries prediction is a work in progress Ludwig can ingest timeseries input feature data and make numerical predictions. Below is an example of a model trained to forecast timeseries at five different horizons.


## Train
ludwig experiment \
--data_csv timeseries_data.csv \
  --model_definition_file model_definition.yaml