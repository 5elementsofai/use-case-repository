# Speaker Verification
This example describes how to use Ludwig for a simple speaker verification task. We assume to have the following data with label 0 corresponding to an audio file of an unauthorized voice and label 1 corresponding to an audio file of an authorized voice. The sample data looks as follows:

audio_path	label
audiodata/audio_000001.wav	0
audiodata/audio_000002.wav	0
audiodata/audio_000003.wav	1
audiodata/audio_000004.wav	1

## Train
ludwig experiment \
--data_csv speaker_verification.csv \
  --model_definition_file model_definition.yaml