# Multi-label classification

## Data
image_path	tags
images/image_000001.jpg	car man
images/image_000002.jpg	happy dog tie
images/image_000003.jpg	boat water

## Train
ludwig experiment \
--data_csv image_data.csv \
  --model_definition_file model_definition.yaml