# One-shot Learning with Siamese Networks

This example can be considered a simple baseline for one-shot learning on the Omniglot dataset. The task is, given two images of two handwritten characters, recognize if they are two instances of the same character or not.

## Train
ludwig experiment \
--data_csv balinese_characters.csv \
  --model_definition_file model_definition.yaml