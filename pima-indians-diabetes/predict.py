import os
import numpy as np
from keras.models import model_from_json

root_path = os.path.dirname(os.path.realpath(__file__))

network_file = open(root_path + "/model.json", 'r')
network = network_file.read()
network_file.close()
model = model_from_json(network)
model.load_weights(root_path + "/model.h5")
print("Loaded model from disk")

data = np.array([[6,148,72,35,0,33.6,0.627,50], [1,85,66,29,0,26.6,0.351,31]])
predictions = model.predict(data)

print('predictions:', predictions)