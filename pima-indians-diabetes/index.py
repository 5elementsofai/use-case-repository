from keras.models import Sequential
from keras.layers import Dense
from keras.models import model_from_json
import numpy as np
import os

class UseCase:
    
    def __init__(self, root_path):
        self.model = self.build_model()
        self.root_path = root_path
    
    def evaluate(self, X, Y):
        return model.evaluate(X, Y, verbose=0)
    
    def evaluate(self, X, Y):
        return self.model.evaluate(X, Y, verbose=0)
    
    def predict(self, X):
        return self.model.predict(np.array(X))
    
    def save(self):
        model_json = self.model.to_json()
        with open(self.root_path + "/model.json", "w") as json_file:
            json_file.write(model_json)
        
        self.model.save_weights(self.root_path + "/model.h5")
        
    
    def load_model(self):
        json_file = open(self.root_path + "/model.json", 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        model = model_from_json(loaded_model_json)
        
        model.load_weights(self.root_path + "/model.h5")
        
        return model
    
    def build_model(self):
        model = Sequential()
        model.add(Dense(12, input_dim=8, activation='relu'))
        model.add(Dense(8, activation='relu'))
        model.add(Dense(1, activation='sigmoid'))
        # Compile model
        model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
        # Fit the model
        #model.fit(X, Y, epochs=150, batch_size=10, verbose=0)
        
        return model



root_path = os.path.dirname(os.path.realpath(__file__))

root_path = os.path.dirname(os.path.realpath(__file__))
use_case = UseCase(root_path)
predictions = use_case.predict([[6,148,72,35,0,33.6,0.627,50], [1,85,66,29,0,26.6,0.351,31]])

print('predictions:', predictions)