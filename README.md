# Ludwig Sandbox

## Install
Just install ludwig and it will be ready to use:

    pip install ludwig


## Train
This is a standard ML competition: Titanic dataset - the best, first challenge for you to dive into Machine Learning.
Use machine learning to create a model that predicts which passengers survived the Titanic shipwreck.

We build a predictive model that answers the question: “what sorts of people were more likely to survive?” 
using passenger data (ie name, age, gender, socio-economic class, etc).
Use training data and model definition to train the model. We are using the titanic dataset:

    ludwig train --data_csv train.csv --model_definition_file model_definition.yaml

Folder results with model directory should be created


## Predict
Use test data and the pre-trained model to predict the output targets:

    ludwig predict --data_csv test.csv --model results/experiment_run/model

Folder results_0 with Survived_predictions.csv, Survived_predictions.npy, Survived_probabilities.csv and Survived_probabilities.npy should be created


## Visualize
Ludwig comes with many visualization options. If you want to look at the learning curves of your model for instance, run:

    ludwig visualize --visualization learning_curves --training_statistics results/experiment_run/training_statistics.json


## Further information
Here you can find some further information about ludwig and the titanic dataset

[![ber Ludwig Tutorial #1 - What is Ludwig and how does it work](http://img.youtube.com/vi/uSOsos2eKHI/0.jpg)](http://www.youtube.com/watch?v=uSOsos2eKHI "Uber Ludwig Tutorial #1 - What is Ludwig and how does it work")

[![Uber Ludwig Tutorial #2 - Working on different data-sets](http://img.youtube.com/vi/NB2aiRKZIok/0.jpg)](http://www.youtube.com/watch?v=NB2aiRKZIok "Uber Ludwig Tutorial #2 - Working on different data-sets")

- https://uber.github.io/ludwig
- https://www.kaggle.com/c/titanic/overview
- https://www.kaggle.com/alexisbcook/titanic-tutorial