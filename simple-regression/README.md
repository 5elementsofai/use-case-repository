# Simple Regression: Fuel Efficiency Prediction

This example replicates the Keras example at https://www.tensorflow.org/tutorials/keras/basic_regression to predict the miles per gallon of a car given its characteristics in the Auto MPG dataset.

## Data
MPG	Cylinders	Displacement	Horsepower	Weight	Acceleration	ModelYear	Origin
18.0	8	307.0	130.0	3504.0	12.0	70	1
15.0	8	350.0	165.0	3693.0	11.5	70	1
18.0	8	318.0	150.0	3436.0	11.0	70	1
16.0	8	304.0	150.0	3433.0	12.0	70	1

## Train
ludwig experiment \
  --data_csv ./auto-mpg.csv \
  --model_definition_file ./model_definition.yaml