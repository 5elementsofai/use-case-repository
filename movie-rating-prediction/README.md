# Movie rating prediction

year	duration	nominations	categories	rating
1921	3240	0	comedy drama	8.4
1925	5700	1	adventure comedy	8.3
1927	9180	4	drama comedy scifi	8.4

## Train
ludwig experiment \
--data_csv movie_ratings.csv \
  --model_definition_file model_definition.yaml