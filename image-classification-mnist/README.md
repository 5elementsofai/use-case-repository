# Image Classification (MNIST)

## Prepare
git clone https://github.com/myleott/mnist_png.git
cd mnist_png/
tar -xf mnist_png.tar.gz
cd mnist_png/

import os
for name in ['training', 'testing']:
    with open('mnist_dataset_{}.csv'.format(name), 'w') as output_file:
        print('=== creating {} dataset ==='.format(name))
        output_file.write('image_path,label\n')
        for i in range(10):
            path = '{}/{}'.format(name, i)
            for file in os.listdir(path):
                if file.endswith(".png"):
                    output_file.write('{},{}\n'.format(os.path.join(path, file), str(i)))


## Train
ludwig train \
  --data_train_csv <PATH_TO_MNIST_DATASET_TRAINING_CSV> \
  --data_test_csv <PATH_TO_MNIST_DATASET_TEST_CSV> \
  --model_definition_file model_definition.yaml