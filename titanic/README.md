# Kaggle's Titanic: Predicting survivors

This example describes how to use Ludwig to train a model for the kaggle competition, on predicting a passenger's probability of surviving the Titanic disaster.


# Train
ludwig experiment \
  --data_csv ./train.csv \
  --model_definition_file ./model_definition.yaml
  