import json
import numpy as np
import pandas as pd
import csv
from ludwig.api import LudwigModel
from pandas.errors import ParserError


def read_csv(data_fp, header=0, nrows=None, skiprows=None):
    """
    Helper method to read a csv file. Wraps around pd.read_csv to handle some
    exceptions. Can extend to cover cases as necessary
    :param data_fp: path to the csv file
    :param header: header argument for pandas to read the csv
    :param nrows: number of rows to read from the csv, None means all
    :param skiprows: number of rows to skip from the csv, None means no skips
    :return: Pandas dataframe with the data
    """

    separator = ','
    with open(data_fp, 'r', encoding="utf8") as csvfile:
        try:
            dialect = csv.Sniffer().sniff(csvfile.read(1024 * 100),
                                          delimiters=[',', '\t', '|'])
            separator = dialect.delimiter
        except csv.Error:
            # Could not conclude the delimiter, defaulting to comma
            pass

    try:
        df = pd.read_csv(data_fp, sep=separator, header=header,
                         nrows=nrows, skiprows=skiprows)
    except ParserError:
        print('Failed to parse the CSV with pandas default way,'
              ' trying \\ as escape character.')
        df = pd.read_csv(data_fp, sep=separator, header=header, escapechar='\\',
                         nrows=nrows, skiprows=skiprows)

    return df


# train a model
'''
with open("./model_definition.json") as json_file:
    model_definition = json.load(json_file)
    
    model = LudwigModel(model_definition)
    train_stats = model.train(data_csv='./train.csv')
    
    print(train_stats)
'''

model_path = './results/experiment_run_0/model'
model = LudwigModel.load(model_path)


#df = read_csv('./test.csv')

data = [
    [
        "892", "3", "Kelly, Mr. James", "male", 34.5, 0, 0, 330911, 7.8292, "", "Q"
    ]
]
columns = [
    "PassengerId", "Pclass", "Name", "Sex", "Age",
    "SibSp", "Parch", "Ticket", "Fare", "Cabin", "Embarked"
]
df = pd.DataFrame(data=data, columns=columns)
predictions = model.predict(data_df=df)
print(predictions)
