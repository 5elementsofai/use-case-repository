
import pandas as pd
import os

df = pd.read_csv('./train.tsv', sep='\t', header=0, names=['caption', 'image_path'])

first_image_path = df['image_path'][0]
print(first_image_path)
print(os.path.isabs(first_image_path))