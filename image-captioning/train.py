
import pandas as pd
import os

df = pd.read_csv('./train.tsv', sep='\t', header=0, names=['caption', 'image_path'])

first_image_path = df['image_path'][0]
print(first_image_path)
print(os.path.isabs(first_image_path))

'''
from ludwig.api import LudwigModel

model_definition = {
    'input_features': [
        {
            'name': 'image_path',
            'type': 'image',
            'encoder': 'stacked_cnn'
        }
    ],
    
    'output_features': [
        {
            'name': 'caption',
            'type': 'text',
            'level': 'word',
            'decoder': 'generator',
            'cell_type': 'lstm'
        }
    ]
}
model = LudwigModel(model_definition)
train_stats = model.train(df)

print(train_stats)
'''